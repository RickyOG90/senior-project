//Files to be imported
import java.io.FileInputStream
import java.io.InputStream

import opennlp.tools.tokenize._
import opennlp.tools.sentdetect._
import opennlp.tools.postag._
import opennlp.tools.lemmatizer.DictionaryLemmatizer

//import scala.swing._
import scala.io.Source
//import scala.collection.immutable.Map
//import scala.collection.mutable.AbstractBuffer
import scala.collection.mutable.ArrayBuffer

object sample2 {
  //Sentence Detector --Return an Array of strings
  def sentence_detect(data:String):Array[String]={
     var modelIn:InputStream = null
     try{
       modelIn =  new FileInputStream(s"C:/Users/ricca/Documents/en-sent.bin")
       val sentence_model:SentenceModel = new SentenceModel(modelIn)
       val sentence_detect: SentenceDetectorME = new SentenceDetectorME(sentence_model)
       return sentence_detect.sentDetect(data)
     }
     catch{
       case ex: Exception => {ex.printStackTrace(); null}
     }
     finally{
       modelIn.close()
     }
  }
  
  //Tokenizer --Return an Array of strings
  def tokenizer_detect(data:String):Array[String]={
    var modelIn:InputStream = null
     try{
       modelIn =  new FileInputStream(s"C:/Users/ricca/Documents/en-token.bin")
       val tokenizer_model:TokenizerModel = new TokenizerModel(modelIn)
       val tokenize_detect: Tokenizer = new TokenizerME(tokenizer_model)
       return tokenize_detect.tokenize(data)
     }
    catch{
      case ex:Exception => {ex.printStackTrace(); null}
    }
    finally{
      modelIn.close()
    }
  }

   //Post tagger -- Return an Array of strings
  def postag_detect(data:Array[String]):Array[String]={
    var modelIn:InputStream = null
    try{
      modelIn =  new FileInputStream(s"C:/Users/ricca/Documents/en-pos-maxent.bin")
      val postag_model: POSModel= new POSModel(modelIn)
      val postag_detect: POSTaggerME = new POSTaggerME(postag_model)
      return postag_detect.tag(data)
    }
    catch{
      case ex: Exception =>{ex.printStackTrace(); null}
    }
    finally{
      modelIn.close()
    }
  }
  
  //Lemmatizer -- Return an Array of string
  def lemmatizer_detect(tokens:Array[String], taggs:Array[String]):Array[String]={
    var dictionaryLemmatizer:InputStream = null
    try{
      dictionaryLemmatizer= new FileInputStream(s"C:/Users/ricca/Documents/lemmatizer-dict.txt")
      val lemmatizer:DictionaryLemmatizer = new DictionaryLemmatizer(dictionaryLemmatizer)
      val lem = lemmatizer.lemmatize(tokens,taggs)
      return lem
    }
    catch{
      case ex: Exception =>{ex.printStackTrace(); null}
    }
    finally{
      dictionaryLemmatizer.close()
    }   
  }
  
  //Lesk algorithm --Currently returns string of word id. 
  //Parameters are word dictionary, gloss (meaning and examples), sentence to be check and post tags
  def simple_lesk(words_dict:Map[String, String], gloss:Map[String,String], sentence:Array[String], pos_t:Array[String]):String=
  {
    var recent =""
    var best_sense =""
    var max_overlap =0
    val context:Array[String] = sentence
    val tagg:Array[String] = pos_t //Post tags (none, noun, verb, adverb, adjective)
    
    var count:Int =0
    for(word<-context)
    {
      if(tagg(count)=="None" || words_dict.get(word) == None) //Checks for stop words by seeing if word not found or post tag not found
        println("stop word: " +word) //Prints stop word. This can be remove, I only use it for testing
      else{
      var id_# = words_dict.get(word).toList(0).split(" ") //gets all the ID# of a word in the dictionary or word_dict
      //for each ID# checked, perform lesk
      for(id_ <-id_#){
        println(id_(id_.size-1).toString() +" "+tagg(count)) //Testing purpose. 
        if(id_(id_.size-1).toString() == tagg(count)){ //finds only the ids with the ending same as the post tag. Example 12433n so the ending is noun
          val gloss_example = gloss.get(id_) //gets the meaning and example for that ID#
          val split_gloss = gloss_example.toList(0).split(" ") //Divides string into individual words 
          val signature = split_gloss.toSet //converts to set
          val overlap = signature.intersect(context.toSet).size //intersection of sets
          if (overlap>=max_overlap){
            max_overlap = overlap
            recent = id_
          }
        }
      }
      if(max_overlap == 0)
        best_sense+= id_#(0).+(" ")
      else
        best_sense+= recent.+(" ")
      max_overlap =0
      }
      count+=1
    }
    println(best_sense)
    return best_sense  
  }
  

  def main(args: Array[String])
  {
    val open_file = Source.fromFile("C:/Users/ricca/Documents/SentiWordNet_3.0.0_20130122.txt")
       
    val POS_LOC = 0      
    val ID_LOC = 1
    val POSS_LOC = 2
    val NEGS_LOC = 3
    val WORD_LOC = 4
    val GLOSS_LOC = 5
    val START = 27        //Lines to skip in the datafile before reading data
    
    var words_dict:Map[String, String] = Map()    //first string is words within database file, second string is ID. Example able 123456a
    var ID_Scores:Map[String, String] = Map()    //first string is ID#, second string is postive and negative scores. 123456a 0.12 0.1
    var Gloss:Map[String, String] = Map()        //first string is ID#, second string is definitions and examples

    val start_t:Long = System.currentTimeMillis() //Start time to load wordnet into memory
    for (word_1<- open_file.getLines().drop(START)){
      //splits the words from #1 to single words but I need to do it in a tree if word repeats or not
      val temp = (word_1.toString().split("\t"))
      for(word_2<-0 until temp(WORD_LOC).split(" ").length by 1){
        var test12 =temp(WORD_LOC).split(" ")(word_2).split("#")(POS_LOC)
        if(words_dict.contains(test12))words_dict=words_dict + (test12->(words_dict.get(test12).mkString("")+" "+temp(ID_LOC).+(temp(POS_LOC))))
        else words_dict= words_dict + ((test12,temp(ID_LOC)+temp(POS_LOC)))    
      }    
      ID_Scores=ID_Scores+((temp(ID_LOC)+temp(POS_LOC), temp(POSS_LOC)+" "+temp(NEGS_LOC)))
      Gloss=Gloss +((temp(ID_LOC)+temp(POS_LOC),temp(GLOSS_LOC)))
    }
    val end_t:Long = System.currentTimeMillis()
    open_file.close()
    
    val NOUNS:Set[String] = Set("NN","NNS","NNP","NNPS")
    val ADJECTIVE:Set[String] = Set("JJ","JJR","JJS")
    val ADVERB:Set[String] =Set("RB", "RBR", "RBS","RP")
    val VERB:Set[String] =Set("VB","VBN","VBZ","VBP","VBD","VBG")
    
    
    val test_file = Source.fromFile("C:/Users/ricca/Documents/test_case.txt") //open text file
    var test_cases:ArrayBuffer[String] = ArrayBuffer() //array of test cases. use for testing right now
    var test_cases_p_s: ArrayBuffer[Array[String]] = ArrayBuffer() // test cases divided into sentences if paragraphs 
    //Reads test cases line by line
    for (n<-test_file.getLines()){
      test_cases+=(n)
      test_cases_p_s+= sentence_detect(n)
    }
    
    //var post_select:Array[String] = Array()
    
    for(count <- test_cases_p_s){  //iterates through test case sentence(s). test case 1,t test 2 case 2, .... etc
      for(n<-count)  //
      {
        var post_done = postag_detect(tokenizer_detect(n)) //post tags of every word in test case
        val sentence_lemma = lemmatizer_detect(tokenizer_detect(n), post_done) // lemmatizer of each word
        //for each word in test case, check if it is noun, verb, adjective, adverb or none then assign n,v,a,r or None. Run lesk on the word
        for (tag<- post_done)
          if (NOUNS.intersect(Set(tag)).size != 0)
            post_done(post_done.indexOf(tag)) = "n"
          else if (ADJECTIVE.intersect(Set(tag)).size != 0)
            post_done(post_done.indexOf(tag)) = "a"
          else if (ADVERB.intersect(Set(tag)).size != 0)
            post_done(post_done.indexOf(tag)) = "r"
          else if (VERB.intersect(Set(tag)).size != 0)
            post_done(post_done.indexOf(tag)) = "v"
          else
            post_done(post_done.indexOf(tag)) = "None";
              
        simple_lesk(words_dict, Gloss, sentence_lemma/*tokenizer_detect(n.toLowerCase())*/, post_done)   
      }
    }
  } 
}