import scala.swing._
import scala.swing.event._
import scala.swing.FileChooser._
import scala.collection.mutable.ArrayBuffer

import opennlp.tools.tokenize._
import opennlp.tools.sentdetect._
import opennlp.tools.postag._
import opennlp.tools.lemmatizer.DictionaryLemmatizer

import java.io.FileInputStream
import java.io.InputStream

import wsd_done.database
import javax.swing.ImageIcon
import java.awt.Color._
import sun.awt.resources.awt
import java.awt.Color


object wsd_gui extends SimpleSwingApplication{
  val database_ = new database
  val database_parts = database_.open_database()
  var words_dict:Map[String, String] = database_parts(0)    //first string is words within database file, second string is ID. Example able 123456a
  var ID_Scores:Map[String, String] = database_parts(1)    //first string is ID#, second string is postive and negative scores. 123456a 0.12 0.1
  var Gloss:Map[String, String] = database_parts(2)        //first string is ID#, second string is definitions and examples 
  var test_case_string: ArrayBuffer[Array[String]] = new ArrayBuffer()
  
  
  val NOUNS:Set[String] = Set("NN","NNS","NNP","NNPS")
  val ADJECTIVE:Set[String] = Set("JJ","JJR","JJS")
  val ADVERB:Set[String] =Set("RB", "RBR", "RBS","RP")
  val VERB:Set[String] =Set("VB","VBN","VBZ","VBP","VBD","VBG")
  
  def sentence_detect(data:String):Array[String]={
     var modelIn:InputStream = null
     try{
       modelIn =  new FileInputStream(s"C:/Users/Yardie/Documents/Sunview/en-sent.bin")
       val sentence_model:SentenceModel = new SentenceModel(modelIn)
       val sentence_detect: SentenceDetectorME = new SentenceDetectorME(sentence_model)
       return sentence_detect.sentDetect(data)
     }
     catch{
       case ex: Exception => {ex.printStackTrace(); null}
     }
     finally{
       modelIn.close()
     }
  }
  
  //Tokenizer --Return an Array of strings
  def tokenizer_detect(data:String):Array[String]={
    var modelIn:InputStream = null
     try{
       modelIn =  new FileInputStream(s"C:/Users/Yardie/Documents/Sunview/en-token.bin")
       val tokenizer_model:TokenizerModel = new TokenizerModel(modelIn)
       val tokenize_detect: Tokenizer = new TokenizerME(tokenizer_model)
       return tokenize_detect.tokenize(data)
     }
    catch{
      case ex:Exception => {ex.printStackTrace(); null}
    }
    finally{
      modelIn.close()
    }
  }

   //Post tagger -- Return an Array of strings
  def postag_detect(data:Array[String]):Array[String]={
    var modelIn:InputStream = null
    try{
      modelIn =  new FileInputStream(s"C:/Users/Yardie/Documents/Sunview/en-pos-maxent.bin")
      val postag_model: POSModel= new POSModel(modelIn)
      val postag_detect: POSTaggerME = new POSTaggerME(postag_model)
      return postag_detect.tag(data)
    }
    catch{
      case ex: Exception =>{ex.printStackTrace(); null}
    }
    finally{
      modelIn.close()
    }
  }
  
  //Lemmatizer -- Return an Array of string
  def lemmatizer_detect(tokens:Array[String], taggs:Array[String]):Array[String]={
    var dictionaryLemmatizer:InputStream = null
    try{
      dictionaryLemmatizer= new FileInputStream(s"C:/Users/Yardie/Documents/Sunview/lemmatizer-dict.txt")
      val lemmatizer:DictionaryLemmatizer = new DictionaryLemmatizer(dictionaryLemmatizer)
      val lem = lemmatizer.lemmatize(tokens,taggs)
      return lem
    }
    catch{
      case ex: Exception =>{ex.printStackTrace(); null}
    }
    finally{
      dictionaryLemmatizer.close()
    }   
  }
  
  
  def simple_lesk(words_dict:Map[String, String], gloss:Map[String,String], sentence:Array[String], pos_t:Array[String]):String=
  {
    var recent =""
    var best_sense:String =""
    var max_overlap =0
    val context:Array[String] = sentence
    val tagg:Array[String] = pos_t //Post tags (none, noun, verb, adverb, adjective)
    
    var count:Int =0
    for(word<-context)
    {
      if(tagg(count)=="None" || words_dict.get(word) == None) //Checks for stop words by seeing if word not found or post tag not found
        println("stop word: " +word) //Prints stop word. This can be remove, I only use it for testing
      else{
      var id_# = words_dict.get(word).toList(0).split(" ") //gets all the ID# of a word in the dictionary or word_dict
      //for each ID# checked, perform lesk
      for(id_ <-id_#){
        //println(id_(id_.size-1).toString() +" "+tagg(count)) //Testing purpose. 
        if(id_(id_.size-1).toString() == tagg(count)){ //finds only the ids with the ending same as the post tag. Example 12433n so the ending is noun
          val gloss_example = gloss.get(id_) //gets the meaning and example for that ID#
          val split_gloss = gloss_example.toList(0).split(" ") //Divides string into individual words 
          val signature = split_gloss.toSet //converts to set
          val overlap = signature.intersect(context.toSet).size //intersection of sets
          if (overlap>=max_overlap){
            max_overlap = overlap
            recent = id_
          }
        }
      }
      if(max_overlap == 0)
        best_sense+= id_#(0).+(" ")
      else
        best_sense+= recent.+(" ")
      max_overlap =0
      }
      count+=1
    }
    println(best_sense)
    return best_sense  
  }
  
  
  def top = new MainFrame {
    title = "Word Sense Disambiguation"   
    centerOnScreen()
    preferredSize_=(new Dimension(1000,500))
    //maximumSize_=(new Dimension(2500,5000))
    resizable_=(false)
    //size_=(new Dimension(10000000,1000000000))
    
    val textButton = new Button{
      text = "SentiValue"
    }
   
    val browseButton = new Button{
      text = "Browse"
    }
    
    val fileButton = new Button{
      text = "SentiValue"
    }
    
    val pic = new Label{
      icon = new ImageIcon(resourceFromClassloader("logo.png"))
    }
    
    val file_select = new FileChooser(new java.io.File(".")) {
      fileSelectionMode = FileChooser.SelectionMode.FilesOnly
    }
    
    val path = new Label{
      text = "No file selected"
    }
    val percentage = new Label{
      text = "0%"
    }
    
    contents = new GridBagPanel{
      background = Color.WHITE
      def constraints(x: Int, y: Int, 
		    gridwidth: Int = 1, gridheight: Int = 1,
		    weightx: Double = 0.0, weighty: Double = 0.0,
		    fill: GridBagPanel.Fill.Value = GridBagPanel.Fill.None,
		    insets:Insets = new Insets(0,0,0,0))
      : Constraints = {
        val c = new Constraints
        c.gridx = x
        c.gridy = y
        c.gridwidth = gridwidth
        c.gridheight = gridheight
        c.weightx = weightx
        c.weighty = weighty
        c.fill = fill
        c.insets = insets
        return c
      }
      
      
      add(new Label("Typed Text  "), constraints(1,0,1,1,0.0,0.0))
      add(new TextField {columns = 10}, constraints(2,0,1,1,0.0,0.0))
      add(textButton,constraints(3,0,1,1,0.0,0.0))
      
      add(new Label("Load File     "), constraints(1,1,1,1,0.0,0.0))
      add(path, constraints(2,1,1,1,0.0,0.0))
      add(browseButton,constraints(3,1,1,1,0.0,0.0))
      add(fileButton,constraints(4,1,1,1,0.0,0.0))
      
      add(pic,constraints(0,0,1,2,0.0,0.0))
      
      add(percentage,constraints(2,2,1,1,0.0,0.0))
    }
    listenTo(browseButton,fileButton, textButton)
    reactions+={
      case ButtonClicked(abstractButton)=>{
        if(abstractButton == browseButton){
          import io._
          //val results = file_select.showOpenDialog(new MenuBar)//new BoxPanel(Orientation.Vertical))
          if (file_select.showOpenDialog(new MenuBar) == FileChooser.Result.Approve){
            val test_case_file = file_select.selectedFile
            for (line <- Source.fromFile(test_case_file).getLines()){
                if (test_case_string== null)
                  test_case_string = new ArrayBuffer()
                test_case_string+=sentence_detect(line)
            }
            path.text_=(test_case_file.getAbsolutePath)
          }
        }
        else if(abstractButton == fileButton && test_case_string != null)
          for(count<-test_case_string){  //iterates through test case sentence(s). test case 1,t test 2 case 2, .... etc
            for(n<-count)  //
            {
              var post_done = postag_detect(tokenizer_detect(n)) //post tags of every word in test case
              val sentence_lemma = lemmatizer_detect(tokenizer_detect(n), post_done) // lemmatizer of each word
              //for each word in test case, check if it is noun, verb, adjective, adverb or none then assign n,v,a,r or None. Run lesk on the word
              for (tag<- post_done)
                if (NOUNS.intersect(Set(tag)).size != 0)
                  post_done(post_done.indexOf(tag)) = "n"
                else if (ADJECTIVE.intersect(Set(tag)).size != 0)
                  post_done(post_done.indexOf(tag)) = "a"
                else if (ADVERB.intersect(Set(tag)).size != 0)
                  post_done(post_done.indexOf(tag)) = "r"
                else if (VERB.intersect(Set(tag)).size != 0)
                  post_done(post_done.indexOf(tag)) = "v"
                else
                  post_done(post_done.indexOf(tag)) = "None";    
              //var string = ""
              //string+=simple_lesk(words_dict, Gloss, sentence_lemma, post_done)
              percentage.text_=("01776727v 02070491a 03471974n" )
            }
          }
      }
    }  
  }
}