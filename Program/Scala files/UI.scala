/**
  * Created by Yardie on 4/15/2017.
  */

import scala.swing._
import scala.swing.event._
import scala.collection.mutable

import javax.swing.ImageIcon
import java.awt.Color

import scala.util.control.Breaks._
import java.io.File
import java.io.BufferedWriter
import java.io.FileWriter
import java.util.Date


class UI extends MainFrame {
  //creates a database object
  val database_ = new database
  //Creates the directory for the files
  database_.directoryCreate()
  //a list of map of array[string] size is 3
  val database_parts:List[Map[String, mutable.Buffer[String]]] = database_.open_database()


  var Words_Dict:Map[String, mutable.Buffer[String]] = database_parts.head    //first string is words within database file, second string is ID. Example able 123456a
  var ID_Scores:Map[String, mutable.Buffer[String]] = database_parts(1)    //first string is ID#, second string is postive and negative scores. 123456a 0.12 0.1
  var Gloss:Map[String, mutable.Buffer[String]] = database_parts(2)       //first string is ID#, second string is definitions and examples

  var post_tag:mutable.Buffer[String] = new mutable.ArrayBuffer()
  var finish_id:mutable.Buffer[String] = new mutable.ArrayBuffer()

  val word_sense = new wsd
  var lesk_return:List[mutable.Buffer[String]] = List() //return 0 is the IDs, return 1 is the words that are valid

  val opennlp = new detect
  var word_size:Int = -1 //starting word of the sentence
  var group = new ButtonGroup
  var id_group:Int = 2  //starting id
  var start:Boolean =true
  var total_sense:BigDecimal =0
  var enable_back:Boolean = false
  var firstInstance:Int = -1

  //creates generic output file for user inputed text
  val generic_file = new File("c:/WSD/Generic_Output.txt")

  if(!generic_file.exists()){
    //initialized bufferwriter for generic output file
    val currentfile = new BufferedWriter(new FileWriter(generic_file))
    currentfile.write("#This text file contains the detail about user input sentence or paragraph\n")
    currentfile.write("#It is ordered as follow: ID, Word, Score, POS\n")
    currentfile.write("#ID - XXXXX if ID not found or ID\n")
    currentfile.write("#Word - the word of a sentence\n")
    currentfile.write("#Score - score of that word\n")
    currentfile.write("#POS - Part of speech. A complete list of the part of speech can be found at http://www.clips.ua.ac.be/pages/mbsp-tags\n")
    currentfile.write("\n\n\n")
    currentfile.close()
  }

  //Prints generic output file location to command prompt.
  println(generic_file.getAbsolutePath + " : Output File Created ")

    title = "Word Sense Disambiguation"
    //preferredSize_=(new Dimension(800,8000))
    resizable_=(false)

    import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    private def showCloseDialog() {
      database_.update_database(Words_Dict, ID_Scores, Gloss)
      if(lesk_return.nonEmpty && lesk_return.head.nonEmpty)

      System.exit(0)
    }

    val textButton = new Button{
      text = "SentiValue"
      //preferredSize_=(new Dimension(100, 30))
    }
    val user_text = new TextArea{
      columns = 20
      rows = 10
      background = Color.lightGray
      foreground = Color.BLACK
      //preferredSize= new Dimension(200,20)
      lineWrap = true
    }

    val pic = new Label{
      icon = new ImageIcon(getClass.getClassLoader.getResource("logo.png"))
      //preferredSize_=(new Dimension(300,300))
    }

    val time_req = new Label
    {
      text = "Database built in: " + database_parts(3).keys.mkString("")+" ms"
    }
  val load = new Label{
    text = database_.updated
  }

    val scroll = new ScrollPane{
      //preferredSize_=(new Dimension(300,200))
      verticalScrollBarPolicy_=(ScrollPane.BarPolicy.AsNeeded)
      horizontalScrollBarPolicy_=(ScrollPane.BarPolicy.AsNeeded)
      visible_=(false)
    }

    val OkayButton = new Button{text = "Select"; visible_=(false)}
    val Next = new Button{text ="NEXT"; visible_=(false)}
    val Back = new Button{text ="BACK"; visible_=(false)}
    val YesButton = new Button{text = "YES"; visible_=(false)}
    val NoButton = new Button{ text = "NO"; visible_=(false)}
    val Finish = new Button{ text = "Confirm"; visible_=(false)}
    val Question = new Label{text = "Is this correct?";  visible_=(false)}

    val percentage = new Label{
      icon = new ImageIcon(getClass.getClassLoader.getResource("neutral.png"))
      //resizable_= (false)
    }

    contents = new GridBagPanel{
      background = Color.WHITE
      def constraints(x: Int, y: Int,
                      gridwidth: Int = 1, gridheight: Int = 1,
                      weightx: Double = 0.0, weighty: Double = 0.0,
                      fill: GridBagPanel.Fill.Value = GridBagPanel.Fill.None,
                      insets:Insets = new Insets(0,0,0,0))
      : Constraints = {
        val c = new Constraints
        c.gridx = x
        c.gridy = y
        c.gridwidth = gridwidth
        c.gridheight = gridheight
        c.weightx = weightx
        c.weighty = weighty
        c.fill = fill
        c.insets = insets
        c
      }
      add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 120)); background_=(Color.WHITE); contents+=new Label("Text")}, constraints(1,0, fill = GridBagPanel.Fill.Both, insets =  new Insets(0,5,0,0)))
      add(time_req, constraints(6,6, insets =  new Insets(150,5,0,0)))//add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 150)); background_=(Color.WHITE); contents+=time_req}, constraints(6,6, insets =  new Insets(5,5,0,0)))
      add(load, constraints(0,6, insets =  new Insets(150,0,0,5)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 150)); background_=(Color.WHITE); contents+=user_text}, constraints(2,0, insets =  new Insets(5,0,0,0)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(120, 50)); background_=(Color.WHITE); contents+=(textButton)},constraints(3,0, insets =  new Insets(0,5,0,5)))

      add(new GridPanel(1,1){preferredSize_=(new Dimension(300,300)); background_=(Color.WHITE); contents+=pic},constraints(0,0, insets =  new Insets(0,0,0,0))) //sunview picture
      add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 150)); background_=(Color.WHITE); contents+=(percentage)},constraints(2, 1, insets =  new Insets(5,0,0,0)))//smile or sad
      add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 150)); background_=(Color.WHITE); contents+=(Question)},constraints(1, 2))

      add(new GridPanel(1,1){preferredSize_=(new Dimension(120, 50)); background_=(Color.WHITE); contents+=(YesButton)},constraints(3, 2, insets =  new Insets(0,5,0,5)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(120, 50)); background_=(Color.WHITE); contents+=(NoButton)},constraints(4, 2, insets =  new Insets(0,5,0,5)))

      add(new GridPanel(1,1){preferredSize_=(new Dimension(300, 150)); background_=(Color.WHITE); contents+=(scroll)},constraints(2, 2, insets =  new Insets(0,0,0,0)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(120, 50)); background_=(Color.WHITE); contents+=OkayButton},constraints(5, 2, insets =  new Insets(0,5,0,5)))

      add(new GridPanel(1,1){preferredSize_=(new Dimension(120,50)); background_=(Color.WHITE); contents+=(Back)},constraints(2, 3, insets =  new Insets(5,0,0,0)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(120,50)); background_=(Color.WHITE); contents+=(Next)},constraints(2, 4, insets =  new Insets(5,0,0,0)))
      add(new GridPanel(1,1){preferredSize_=(new Dimension(120,50)); background_=(Color.WHITE); contents+=(Finish)},constraints(2, 5, insets =  new Insets(5,0,0,0)))
    }
    listenTo(textButton, YesButton, NoButton, OkayButton, Next, Back, Finish)
    reactions+={
      case ButtonClicked(abstractButton)=>
        if (abstractButton == textButton && user_text.text.nonEmpty) {
          val current_sentences = opennlp.sentence_detect(user_text.text)
          val date = new Date()
          val outfile = new BufferedWriter(new FileWriter("c:/WSD/Generic_Output.txt", true))
          outfile.write(date.toInstant + "\n")
          outfile.close()
          total_sense = 0
          for (sentence <- current_sentences) {
            //Tokenize of a sentence
            val tokenize = opennlp.tokenizer_detect(sentence)
            //tracks if the word was divided into two words also can be removed?
            //var tokenize_change = mutable.Buffer[Int](tokenize.length)
            //Returns the post tag of each word into a buffer of strings
            post_tag = opennlp.postag_detect(tokenize.toArray)
            var temp_tokenize = opennlp.lemmatizer_detect(tokenize.toArray, post_tag)
            //checks if the word is O meaning it's not in lemmatizer then replace it with the correct word cause this correct word will
            //be needed again for doing 2nd POS
            for (index <- tokenize.indices) {
              if(!temp_tokenize(index).contains(" ")){
                temp_tokenize(index) = tokenize(index)
              }
            }
            /*if(temp_tokenize.contains("O")) {
              for (wrong <- temp_tokenize.indices) {
                if (temp_tokenize(wrong) == "O") {
                  temp_tokenize(wrong) = tokenize(wrong)
                }
              }
            }*/
            //splits any words thats aren't split
            temp_tokenize= opennlp.tokenizer_detect(temp_tokenize.mkString(" "))
            val temp_taggs = opennlp.postag_detect(temp_tokenize.toArray)
            //Runs the lesk algorithm and returns Buffer(best_sense, used_words, correctID1, correctID2, correctIDn)
            lesk_return = word_sense.simple_lesk(Words_Dict, Gloss, opennlp.lemmatizer_detect(temp_tokenize.toArray, temp_taggs), word_sense.post_tag_adjust(temp_taggs))
            //lesk_return = word_sense.simple_lesk(Words_Dict, Gloss, opennlp.lemmatizer_detect(tokenize.toArray, post_tag), word_sense.post_tag_adjust(post_tag))
            //holds the scores of all the words selected
            val scores = word_sense.getScores(lesk_return.head, ID_Scores)
            //calculate the total sense of a sentence not paragraph
            var currentsense = word_sense.calculation_posneg(scores)
            //Write to the output file
            database_.outputfile(temp_tokenize, lesk_return.head, word_sense.getScores(lesk_return.head, ID_Scores), temp_taggs, currentsense)
            //database_.outputfile(tokenize, lesk_return.head, word_sense.getScores(lesk_return.head, ID_Scores), post_tag, currentsense)
            //total sense of a paragraph or sentence if single sentence
            total_sense += currentsense
          }
          //takes the average of the total for a single sentence  or paragraph
          if (current_sentences.nonEmpty) {
            total_sense = total_sense / current_sentences.size
            database_.write_total(total_sense)
          }
          //Checks the value of the sentence
          if (total_sense < 50){

            percentage.icon_=(new ImageIcon(getClass.getClassLoader.getResource("sad.png")))
            percentage.text_=(total_sense.toString())
          }
          else if (total_sense > 50){
            percentage.icon_=(new ImageIcon(getClass.getClassLoader.getResource("smile.png")))
            percentage.text_=(total_sense.toString())
          }
          else {
            percentage.icon_=(new ImageIcon(getClass.getClassLoader.getResource("neutral.png")))
            if(lesk_return.isEmpty|| lesk_return.isEmpty|| lesk_return.head.toSet.size == 1 && lesk_return.head.toSet.contains("XXXXX"))
              percentage.text_=("Error Sentence Not Valid or Mispelled Sentence")
            else
              percentage.text_=(total_sense.toString())
          }
          //Ask question at the end. Checks if the lesk algorithm ran, if ran checks if the size of the best sense is not zero then checks if the words are valid
          //changing to set will make the ("XXXXX", "XXXXX") become "XXXXX"
          if (lesk_return.nonEmpty && lesk_return.head.nonEmpty){
            if(lesk_return.head.toSet.size == 1 && !lesk_return.head.toSet.contains("XXXXX") || lesk_return.head.toSet.size >1){
              Question.visible_=(true)
              YesButton.visible_=(true)
              NoButton.visible_=(true)
              //textButton.visible_=(false)
            }
          }
        }
        else if (abstractButton == YesButton || abstractButton == Finish)
        {
          Question.visible_=(false)
          YesButton.visible_=(false)
          NoButton.visible_=(false)
          scroll.visible_=(false)

          Finish.visible_=(false)
          OkayButton.visible_=(false)
          Next.visible_=(false)
          Back.visible_=(false)
          textButton.visible_=(true)
          start = true
          word_size= -1
          id_group = 2
          firstInstance = -1
        }
        else if ((abstractButton == NoButton || abstractButton == Next || abstractButton ==OkayButton ||abstractButton == Back) && word_size <= lesk_return(1).size)
        {
            var radios:mutable.Buffer[RadioButton] =  new mutable.ArrayBuffer()
            breakable{
              if(abstractButton != Back){
                if (abstractButton != OkayButton){
                  while(word_size < lesk_return.head.size-1){
                    println(word_size)
                    println(lesk_return.head.size-1)
                    println(word_size == lesk_return.head.size-1)
                    word_size+=1
                    if(lesk_return.head(word_size) != "XXXXX"){
                      if (firstInstance < 0) firstInstance = word_size
                      break
                    }
                  }
                }
              }
              else {
                while(word_size >= 0){
                  word_size-=1
                  if(lesk_return.head(word_size) != "XXXXX")break}
              }
            }
            Question.visible_=(false)
            YesButton.visible_=(false)
            NoButton.visible_=(false)
            //Next button visible if word is not 1 and not at the end
            if(word_size < lesk_return(1).size-1 && lesk_return(1).size != 1){Next.visible_=(true); Finish.visible_=(false)}
            else{Next.visible_=(false); Finish.visible_=(true)}

            OkayButton.visible_=(true)

            if(start){
              scroll.contents= new BoxPanel(Orientation.Vertical) {
                contents+= new Label{text= lesk_return(1)(word_size); font = new java.awt.Font("Verdana", java.awt.Font.BOLD,22)}
                contents+= new Label{text= "I thought you meant: "+ Gloss(lesk_return.head(word_size)).head +
                                            "     Score= " + (ID_Scores(lesk_return.head(word_size)).head.toDouble
                                            + ID_Scores(lesk_return.head(word_size))(1).toDouble)
                }

                for(id<-lesk_return(id_group)){
                  if(!lesk_return.head.contains(id)){
                    val click = new RadioButton(Gloss(id).head +
                      "\r\n     Score= " + (ID_Scores(id).head.toDouble + (ID_Scores(id)(1).toDouble * -1)))
                    contents+=click
                    radios.append(click)
                  }
                 }
                group.buttons++= radios
              }
              start=false
            }
            else if(abstractButton == Next && word_size < lesk_return(1).size && lesk_return(1).size > 1)
            {

                println(lesk_return(1)(word_size))
                id_group += 1
                enable_back = true
                group.buttons.clear()
                scroll.contents = new BoxPanel(Orientation.Vertical) {
                  contents += new Label(lesk_return(1)(word_size))
                  contents += new Label {
                    text = "I thought you meant: " + Gloss(lesk_return.head(word_size)).head +
                      "     Score= " + (ID_Scores(lesk_return.head(word_size)).head.toDouble
                      + ID_Scores(lesk_return.head(word_size))(1).toDouble)
                  }
                  for (id <- lesk_return(id_group)) {
                    if (!lesk_return.head.contains(id)) {
                      val click = new RadioButton(Gloss(id).head +
                        "\r\n     Score= " + (ID_Scores(id).head.toDouble + (ID_Scores(id)(1).toDouble * -1)))
                      contents += click
                      radios.append(click)
                    }
                  }
                  group.buttons ++= radios
                }
            }
            else if(abstractButton == OkayButton){
              breakable{
                val temp_id: mutable.Buffer[String] = lesk_return(id_group)
                for(id <- temp_id)
                {
                  //find the definitions of the word
                  if((Gloss(id).head+
                    "\r\n     Score= " + (ID_Scores(id).head.toDouble + (ID_Scores(id)(1).toDouble * -1))) == group.selected.get.text)
                  {
                    val temp_array = Gloss(id)
                    //check is sentence already in database
                    if(!temp_array.contains(lesk_return(1).mkString(" ")))
                    {
                      temp_array.append(lesk_return(1).mkString(" "))
                      Gloss+=id->temp_array


                    }
                    break
                  }
                }
              }
            }
            else if(abstractButton == Back && word_size > -1 && lesk_return(1).size != 1){
              Finish.visible_=(false)
              Next.visible_=(true)
              id_group-=1
              group.buttons.clear()
              scroll.contents= new BoxPanel(Orientation.Vertical){
                contents+= new Label(lesk_return(1)(word_size))
                contents+= new Label {
                  text = "I thought you meant: " + Gloss(lesk_return.head(word_size)).head +
                    "     Score= " + (ID_Scores(lesk_return.head(word_size)).head.toDouble
                    + ID_Scores(lesk_return.head(word_size))(1).toDouble)
                }
                for(id<-lesk_return(id_group)){
                  if(!lesk_return.head.contains(id)){
                    val click = new RadioButton(Gloss(id).head + "\r\n     Score= " + (ID_Scores(id).head.toDouble + (ID_Scores(id)(1).toDouble * -1)))
                    contents+=click
                    radios.append(click)
                  }
                }
                group.buttons++= radios
              }
            }
            //Back button visible is size is not 1 or 0
            if(enable_back && lesk_return(1).size != 1 && firstInstance != word_size) {Back.visible_=(true);}
            else{Back.visible_=(false)}
            scroll.visible_=(true)
          }
      }

}
