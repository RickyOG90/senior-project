import scala.math.BigDecimal.RoundingMode
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer
import scala.math.BigDecimal.double2bigDecimal
import scala.math.BigDecimal.int2bigDecimal

class wsd {
  def simple_lesk(words_dict:Map[String, Buffer[String]], gloss:Map[String, Buffer[String]], sentence:Buffer[String], pos_t:Buffer[String]):List[Buffer[String]]=
  {
    var overlap:Int =0  //number of common words found
    var recent =""      //holds the recent ID that has the most common words
    val best_sense:Buffer[String] = new ArrayBuffer()  //holds the best sense of a sentence
    val tagg:Buffer[String] = pos_t   //Post tags (none, noun, verb, adverb, adjective)
    var count:Int =0  //use to iterate through the post tags
    val correctID:Buffer[String] = new ArrayBuffer() //the right IDs picked for a word. example can be man noun or verb but only needs noun so it picks nouns only.
    val used_words:Buffer[String] = new ArrayBuffer() //list of words in sentence found in database
    var final_out:List[Buffer[String]]= List()  //the output of the function. contains Buffer(best_sense, used_words, correctID1, correctID2, correctIDn)
    
    //iterate through a sentence and gets the words of that sentence
    for(word<-sentence)
    {
      //Checks for stop words by seeing if word not found or post tag not found then ID is XXXXX and used words are XXXXX
      if(word == "must")
        println("stop")
      if(tagg(count)=="None" || words_dict.get(word).isEmpty){println("word or part-of-speech: "+ word + " not in database"); best_sense.append("XXXXX"); used_words.append("XXXXX")}
      else
      { 
        used_words.append(word)
        //get all the ID# of a word in the dictionary or word_dict 
        val id_# = words_dict(word)
        //iterate through the IDs
        for(id_ <- id_#)
        {
          //check if the id matches with the part of speech
          if(id_(id_.length-1).toString == tagg(count)){
            correctID.append(id_)
            //check if there are examples of the word.
            val definition_example = gloss(id_)//Gets the definition and examples of a word 
            if(definition_example.size> 1){
              for(example<- definition_example.slice(1, definition_example.length)){
                if(example.split(" ").toSet.intersect(sentence.toSet).size > overlap)
                {
                  overlap = example.split(" ").toSet.intersect(sentence.toSet).size //number of intersected words
                  recent= id_
                }
              }
            }
          }
        }
        if(correctID.nonEmpty){
          final_out= final_out.:+(correctID.clone())       
          if(overlap !=0 )
          {
            best_sense.append(recent)
          }
          else
          {
            best_sense.append(correctID.head)
          }
        }
        else{
          println("Lemmatizer and database sync error. Please check.")
          best_sense.append("XXXXX")
        }
        correctID.clear()
      }
      count += 1
      overlap =0
    }
      final_out = final_out.+:(used_words)
      final_out= final_out.+:(best_sense)
      
      final_out//Buffer(best_sense, used_words, correctID1, correctID2, correctIDn)
  }
  
  //Match the post tags to senti wordnet
  def post_tag_adjust(post_done:Buffer[String]):Buffer[String]={
    val NOUNS:Set[String] = Set("NN","NNS","NNP","NNPS")
    val ADJECTIVE:Set[String] = Set("JJ","JJR","JJS")
    val ADVERB:Set[String] =Set("RB", "RBR", "RBS","RP")
    val VERB:Set[String] =Set("VB","VBN","VBZ","VBP","VBD","VBG", "MD")
    val new_tags = post_done.clone()
    for (tag<- new_tags){
      if (NOUNS.intersect(Set(tag)).nonEmpty)
        new_tags(new_tags.indexOf(tag)) = "n"
      else if (ADJECTIVE.intersect(Set(tag)).nonEmpty)
        new_tags(new_tags.indexOf(tag)) = "a"
      else if (ADVERB.intersect(Set(tag)).nonEmpty)
        new_tags(new_tags.indexOf(tag)) = "r"
      else if (VERB.intersect(Set(tag)).nonEmpty)
        new_tags(new_tags.indexOf(tag)) = "v"
      else
        new_tags(new_tags.indexOf(tag)) = "None"
       }
      new_tags
  }

  def calculation_posneg(ScoreArray:Array[String]):BigDecimal={
    val neutral = 50.0
    var total: BigDecimal = 0.0
    var n= ScoreArray.length
    for(k <- 0 until n){
      if (ScoreArray(k).nonEmpty){
        total += (BigDecimal(ScoreArray(k))*100)+neutral
      }
      else{
        n -= 1
      }
    }
  
    if(n==0) neutral
    else{
      total = total/n
      if (total < 0) 0
      else if(total > 100) 100
      else total.setScale(0, RoundingMode.HALF_EVEN)
    }
  }
  
  def getScores(IDs:Buffer[String], Scores:Map[String, Buffer[String]]):Array[String]={
    var ScoreString = ""
    var n = IDs.size
    var negate:Boolean = false //use to detect if a word is to be negate
    if (n != 0){
      for(id<-IDs){

        if(id == "XXXXX"){
          ScoreString+= "0.0" + " "
        }
        else {
          val pos = Scores(id).head.toDouble
          val neg = Scores(id)(1).toDouble * -1
          //checks if the the word should be negate then turn off negate
          if(negate){
            ScoreString += ((pos + neg) * -1).toString + " "
            negate = false
          }
          else {
            ScoreString += (pos + neg).toString + " "
          }
          ///checks if the id is not and negate the other words after that it. this is done on the next loop.
          if(id == "00024073r"){negate = true}
        }
      }
    }
    ScoreString.split(" ")
  }
}

  
  