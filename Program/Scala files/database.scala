
import scala.collection.mutable
import scala.io.Source
import java.io.PrintWriter
import java.io.FileOutputStream
import java.io.File
import java.io.BufferedWriter
import java.io.FileWriter
import java.io.FileNotFoundException
import java.io.IOException
import java.net._
import sys.process._

class database {
  var updated:String = ""
  var maxID:Int = 0
  def open_database(): List[Map[String, mutable.Buffer[String]]]={
    //URL for database
    var url:String = "https://raw.githubusercontent.com/tashanejm/Senior-Project/master/Database/custom_database.txt"
    //download database file to system
    new URL(url) #> new File("c:/WSD/database.txt")!
    var update:String = null

    val database_file = new File("c:/WSD/database.txt")
    if(!database_file.exists())
    {
      println("database file not found.")
      try{
        val bw = new BufferedWriter(new FileWriter(database_file))
        bw.close()
        println("Database Created.")
        update = "Database updated"
      }
      catch{
        case e: IOException=>println(e+"IO Error")
        case e: FileNotFoundException=> println(e+"File not found error")
      }
    }

    var open_file = Source.fromFile(database_file)
    val POS_LOC = 0      
    val ID_LOC = 1
    val POSS_LOC = 2
    val NEGS_LOC = 3
    val WORD_LOC = 4
    val GLOSS_LOC = 5
    var START = 1
    //val CustomFile = open_file.isEmpty
    var final_time:Long =0
    var timeout:Map[String, mutable.Buffer[String]] = Map()
    
    var words_dict1:Map[String, mutable.Buffer[String]] = Map()
    var ID_Scores1:Map[String, mutable.Buffer[String]] = Map()
    var Gloss1:Map[String, mutable.Buffer[String]] = Map()



    
    //Checks if the custom database is empty
    if(open_file.isEmpty)
    {
      open_file.close()
      url = "https://raw.githubusercontent.com/tashanejm/Senior-Project/master/Database/SentiWordNet_3.0.0_20130122.txt"
      new URL(url) #> new File("c:/WSD/backup-database.txt")!
      val in = getClass.getResourceAsStream("c:/WSD/backup-database.txt")
      updated = "Backup database loaded"
      START = 27 //Lines to skip in the datafile before reading data
      open_file = Source.fromInputStream(in)
      val start_t:Long = System.currentTimeMillis() //Start time to load wordnet into memory
      for (word_1<- open_file.getLines().drop(START)){
        
        //splits the words from #1 to single words
        val tab_split = word_1.toString.split("\t")
        for(word_2 <- tab_split(WORD_LOC).split(" ").indices by 1){//0 until tab_split(WORD_LOC).split(" ").size by 1){
          if(tab_split(WORD_LOC).split(" ")(word_2).contains("#") && !tab_split(WORD_LOC).split(" ")(word_2).contains("_")){//ignores double words
            val extract_word =tab_split(WORD_LOC).split(" ")(word_2).split("#")(POS_LOC)
            val extract_number = tab_split(WORD_LOC).split(" ")(word_2).split("#")(1)
            if(words_dict1.contains(extract_word)) 
            {
              words_dict1(extract_word).append(extract_number+"#"+tab_split(ID_LOC)+tab_split(POS_LOC))
              words_dict1+=extract_word->words_dict1(extract_word).sorted
            }
            else 
            {
              words_dict1+= ((extract_word, mutable.ArrayBuffer(extract_number+"#"+tab_split(ID_LOC)+tab_split(POS_LOC))))
            }
           }
          }
        ID_Scores1+=((tab_split(ID_LOC)+tab_split(POS_LOC), mutable.ArrayBuffer(tab_split(POSS_LOC),tab_split(NEGS_LOC))))
        Gloss1+=((tab_split(ID_LOC)+tab_split(POS_LOC), mutable.Buffer(tab_split(GLOSS_LOC).split("; ")(0))))
       }
      val end_t:Long = System.currentTimeMillis()
      open_file.close()
      val custom_file = new BufferedWriter(new FileWriter(database_file))
      custom_file.write("#This is ordered by ID with the part of speech, positive score, negative score, definition and examples")
       for(key<-words_dict1.keys)
       {
         val temp_list = words_dict1(key)
         var temp_ids:mutable.Buffer[String] = mutable.ArrayBuffer()
         for(buff <-temp_list)
         {
           val id=buff.split("#")(1)
           custom_file.write("\n%s\t%s\t%s\t%s\t%s".format(id, ID_Scores1(id).head,ID_Scores1(id)(1), key, Gloss1(id).head))
           temp_ids+=id
         } 
         words_dict1+=key->temp_ids
        }       
        custom_file.close()
        final_time = end_t-start_t
        println(final_time)
    }
    else
    {
      updated = "Latest database loaded"
       val start_t:Long = System.currentTimeMillis() //Start time to load wordnet into memory
       for (word_1<- open_file.getLines().drop(START))
       {
         val tab_split = word_1.split("\t")
         val id = tab_split(0)
         val posscore = tab_split(1)
         val negscore = tab_split(2)
         val extract_word = tab_split(3)
         val definition = tab_split(4).split(";").toBuffer
         if(words_dict1.contains(extract_word))
         {
           words_dict1(extract_word).append(id)
         }
         else
         {
           words_dict1+= ((extract_word, mutable.ArrayBuffer(id)))
         }
         if(!ID_Scores1.contains(id))
         {
            ID_Scores1+=((id, mutable.ArrayBuffer(posscore,negscore)))
            Gloss1+= ((id, definition))//ArrayBuffer(definition)))
         }
       }
       final_time = System.currentTimeMillis()- start_t
       open_file.close()

    }
    timeout+= ((final_time.toString, mutable.ArrayBuffer(final_time.toString)))
     List(words_dict1,ID_Scores1,Gloss1, timeout)
  }
  def update_database(words:Map[String, mutable.Buffer[String]], scores:Map[String, mutable.Buffer[String]], gloss:Map[String, mutable.Buffer[String]])
  {
    val custom_file = new PrintWriter(new FileOutputStream("c:/WSD/database.txt"),false)
    custom_file.write("#This is ordered by ID with the part of speech, positive score, negative score, definition and examples")

    for(word<-words.keys)
    {
      for(id <-words(word))
      {
        custom_file.write("\n%s\t%s\t%s\t%s\t%s".format(id, scores(id).head,scores(id)(1),word, gloss(id).mkString("; ")))
      }
    }
    custom_file.close()
  }
  def outputfile(current_sentence:mutable.Buffer[String], IDs:mutable.Buffer[String], Scores:Array[String], POS:mutable.Buffer[String], currentsense:BigDecimal){
    var currentfile:BufferedWriter=null
    try {      
      currentfile = new BufferedWriter(new FileWriter("c:/WSD/Generic_Output.txt", true))
      currentfile.write("Sentence: "+current_sentence.mkString(" ")+"\n")
      for (j <- current_sentence.indices)
        currentfile.write("("+IDs(j)+", "+current_sentence(j) +", " + Scores(j) + ", " + POS(j)+")")        
      //Prints the end bracket for the sentence and current sense 
      //score for the sentence to the generic file
      currentfile.write("} : SentenceSense: " + currentsense + "\r\n")
      currentfile.close()
    }
    catch{case e: FileNotFoundException => println(e+"Can't write to output file.")}
    //println("done")
  }
  def write_total(total_sense:BigDecimal){
    val currentfile = new BufferedWriter(new FileWriter("c:/WSD/Generic_Output.txt", true))
    //write to generic file
    currentfile.write("TotalSense: " + total_sense + "\n")
    //closes the generic file
		currentfile.close()
  }
  //Creates a directory in c:\
  def directoryCreate(){
    //creates a directory variable location
    val theDir: File = new File("c:/WSD/")
    //true or false for the directory 
    var result: Boolean = false
    //checks if directory exists
    if (!theDir.exists()){
      println("Directory not found")
      try{
        //Create directory
        theDir.mkdir()
        //set to true since directory was created
        result = true
      }
      catch{
        case e: IOException => println(e)
        case e: FileNotFoundException => println(e)
      }
    }
    if(result) {
      println("Directory Created.")
    }
  }


}