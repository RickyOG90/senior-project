import opennlp.tools.tokenize._
import opennlp.tools.sentdetect._
import opennlp.tools.postag._
import opennlp.tools.lemmatizer.DictionaryLemmatizer
import java.io.InputStream
import scala.collection.mutable.Buffer

class detect {
  def sentence_detect(data:String):Buffer[String]={
     var modelIn:InputStream = null
     try{
       //modelIn =  new FileInputStream(s"en-sent.bin")
       modelIn = getClass.getResourceAsStream("en-sent.bin")
       val sentence_model:SentenceModel = new SentenceModel(modelIn)
       val sentence_detect: SentenceDetectorME = new SentenceDetectorME(sentence_model)
       
       sentence_detect.sentDetect(data).toBuffer
     }
     catch{
       case ex: Exception => ex.printStackTrace(); null
     }
     finally{
       modelIn.close()
     }
  }
  
  //Tokenizer --Return an Array of strings
  def tokenizer_detect(data:String):Buffer[String]={
    var modelIn:InputStream = null
     try{
       //modelIn =  new FileInputStream(s"en-token.bin")
       modelIn = getClass.getResourceAsStream("en-token.bin")
       val tokenizer_model:TokenizerModel = new TokenizerModel(modelIn)
       val tokenize_detect: Tokenizer = new TokenizerME(tokenizer_model)
       tokenize_detect.tokenize(data).toBuffer
     }
    catch{
      case ex:Exception => ex.printStackTrace(); null
    }
    finally{
      modelIn.close()
    }
  }

   //Post tagger -- Return an Array of strings
  def postag_detect(data:Array[String]):Buffer[String]={
    var modelIn:InputStream = null
    try{
      //modelIn =  new FileInputStream(s"en-pos-maxent.bin")
      modelIn = getClass.getResourceAsStream("en-pos-maxent.bin")
      val postag_model: POSModel= new POSModel(modelIn)
      val postag_detect: POSTaggerME = new POSTaggerME(postag_model)
      postag_detect.tag(data).toBuffer
    }
    catch{
      case ex: Exception =>ex.printStackTrace(); null
    }
    finally{
      modelIn.close()
    }
  }
  
  //Lemmatizer -- Return an Array of string
  def lemmatizer_detect(tokens:Array[String], taggs:Buffer[String]):Buffer[String]={
    var dictionaryLemmatizer:InputStream = null
    try{
      dictionaryLemmatizer = getClass.getResourceAsStream("en-lemmatizer.bin")
      val lemmatizer:DictionaryLemmatizer = new DictionaryLemmatizer(dictionaryLemmatizer)
      val lem = lemmatizer.lemmatize(tokens,taggs.toArray)
      lem.toBuffer
    }
    catch{
      case ex: Exception =>ex.printStackTrace(); null
    }
    finally{
      dictionaryLemmatizer.close()
    }   
  }
  
}